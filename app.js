var http = require("http");
var fs = require("fs");

http.createServer(function(req, res){
    var getUri = req.url;
    var code = "";
    var html = "";
    if(getUri != "/favicon.ico"){
        if(getUri == "/"){
            code = 200;
            html = "./views/index.html";
        }else if(getUri == "/profile"){
            code = 200;
            html = "./views/profile.html";
        }else {
            code = 404;
            html = "./views/404.html";
        }
    }
    res.writeHead(code, {"Content-Type":"text/html"});
    fs.createReadStream(html).pipe(res);
}).listen(3000);

console.log("Server is running ...");
